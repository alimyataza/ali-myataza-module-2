class App {
  String name;
  List categories;
  String developer;  
  int year;
  
  App(this.name, this.categories,this.developer,this.year);
  
  @override
  String toString() {
    return 'App Name: $name\nSector: $categories\nDeveloper: $developer\nYear Won: $year';
  }
  
  String getAppName() {
    var appName = name.toUpperCase();
    return appName;
  }
}

enum Category { 
   bestConsumerSolution,
   bestEnterpriseSolution, 
   bestAfricanSolution, 
   bestInnovativeSolution, 
   bestGamingSolution, 
   bestHealthSolution, 
   bestAgriculturalSolution, 
   bestEducationalSolution, 
   bestFinancialSolution, 
   bestHackathonSolution, 
   bestSouthAfricanSolution, 
   bestCampusCupSolution      
}

void main() {
  
  var winnerCategories = [Category.bestSouthAfricanSolution.name,
                    Category.bestGamingSolution.name,
                    Category.bestEducationalSolution.name];
  
  var winner = App("Ambani Africa",winnerCategories,"Mukundi Lambani",2021);
  
  print(winner.toString());
  print(winner.getAppName());
  
}
