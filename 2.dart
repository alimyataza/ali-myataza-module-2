import 'dart:collection';

void main() {
  var overallWinners = {2021 : 'Ambani Africa'
                        ,2020 : 'EasyEquities'
                        ,2019 : 'Naked Insurance'
                        ,2018 : 'Khula'
                        ,2017 : 'Shyft'
                        ,2016 : 'Domestly'
                        ,2015 : 'WumDrop'
                        ,2014 : 'LIVE Inspect'
                        ,2013 : 'SnapScan'
                        ,2012 : 'FNB Banking App'};

  
  
  // a)
  var sortByValue = SplayTreeMap<int, String>.from(
      overallWinners, (key1, key2) => overallWinners[key1]!.compareTo(overallWinners[key2]!)); 
  
  print("*** The winning apps of the MTN Business App of the Year Awards***");  
  for (var winner in sortByValue.values) {
    print(winner);
  }
  
  // b)
  print("\n*** The winning app of 2017 ***");
  print(overallWinners[2017]);
  
  print("\n*** The winning app of 2018 ***");
  print(overallWinners[2018]);

  // c)
  print("\n*** The number of winning apps ***");
  print(overallWinners.length);
  
}
